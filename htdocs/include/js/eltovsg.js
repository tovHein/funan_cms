// START DEVICE Page
function openEltovPopup(){
 $('#popup').addClass('active');
 $('html').addClass('popupActive');
}

function openEltovEditPlaylist(that){
    var title = $(that).parent().parent().find('.title').html();
    console.log(title);
    $('#popup').find('input[name="playlistName"]').val(title);
    $('#popup').addClass('active');
    $('html').addClass('popupActive');
}
// END DEVICE Page

$( document ).ready(function() {


// START PRODUCT Detail Page
$('.input-daterange-timepicker').daterangepicker({
    timePicker: false,
    format: 'MM/DD/YYYY h:mm A',
    timePickerIncrement: 30,
    timePicker12Hour: true,
    timePickerSeconds: false,
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-danger',
    cancelClass: 'btn-inverse'
});


$('.edit-product').click(function(){
    $('#popup').addClass('active');
    $('html').addClass('popupActive');
});

$('.close-edit-popup').click(function(){
    $('#popup').removeClass('active');
    $('html').removeClass('popupActive');
});

$("input[name='tch1']").TouchSpin({
    min: 0,
    max: 100,
    step: 1,
    decimals: 2,
    boostat: 5,
    maxboostedstep: 10,
});

// END PRODUCT Detail Page 


// START Shop List
$('#eltov-dataTable').footable();
$('#eltov-dataTable-nopagination').footable({paginate:false});
// END Shop List


//  START Shop Detail Page
$('.clockpicker').clockpicker({
    donetext: 'Done',
}).find('input').change(function() {
    console.log(this.value);
});


$('.24hours input').on('ifChecked', function(event){
    $(this).closest('.row').find('.select-time, .switch-select').fadeOut();
    console.log('check');
});

$('.24hours input').on('ifUnchecked', function(event){
    $(this).closest('.row').find('.select-time, .bt-switch').fadeIn();
});


$(".bt-switch input[type='checkbox']").bootstrapSwitch();

$(".bt-switch input[type='checkbox']").on('switchChange.bootstrapSwitch', function (event, state) {
    if( event.target.checked == true) {
     $(this).closest('.row').find('.select-time, .24hours').fadeIn();
 }else{
     $(this).closest('.row').find('.select-time, .24hours').fadeOut();
 }

}); 

$('#show-date-advance').on('click', function(){
    if( $(this).hasClass('active') ){
        $(this).removeClass('active');
        $('#switch-components').slideUp();
        $('#operation-hour').slideDown();

    }else{
        $(this).addClass('active');
        $('#switch-components').slideDown();
        $('#operation-hour').slideUp();
    }

});

//  END Shop Detail Page

});






// START Program Page

function editProgram(){
    $('#popup').addClass('active');
    $('html').addClass('popupActive');
}


// END Program Page